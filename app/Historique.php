<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historique extends Model
{
    protected $fillable = ['employe_id', 'appareil_id', 'debut', 'fin'];

}
