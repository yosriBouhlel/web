<?php

namespace App\Http\Controllers;

use App\User;
use App\Appareil;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        

       if ($request->has('password')) {

            $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);
           
       }else{

            $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ]);

       }


        $appareils = Appareil::all();


        return redirect('/appareils')->with(compact('appareils'));
    }



         /************************************ Api methods ************************************/



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeApi(Request $request)
    {
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
                ]);

        return response()->json(Response::HTTP_OK);
    }







    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateApi(Request $request)
    {
        
       $user = JWTAuth::parseToken()->authenticate();

       if ($request->has('password')) {

            $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);
           
       }else{

            $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ]);

       }





        return response()->json(Response::HTTP_OK);
    }   


    
    /**
     * Delete user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteApi(Request $request)
    {
        $user = JWTAuth::parseToken()->authenticate();

        if($user->delete())

            return response()->json(Response::HTTP_OK);
        else
            return response()->json(Response::HTTP_INTERNAL_SERVER_ERROR);
    }


    

    /**
     * Get user's details
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userDetailsApi(Request $request)    
    {        
        $user = JWTAuth::parseToken()->authenticate();

        $details = User::find($user->id);

        return response()->json(['details' => $details],Response::HTTP_ACCEPTED);   
    }  


}
