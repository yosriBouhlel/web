@component('mail::message')
# Welcome, {{ $user['name'] }} <br>

Email: {{$user['email']}}<br>
Password: {{$user['password']}}<br>

You can access to the login page from <a href="localhost:8000">here</a>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
