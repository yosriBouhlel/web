@extends('layouts.app')

@section('title')

Liste des appareils

@endsection

@section('menus')

<ul class="nav navbar-nav">
  <li class="active" ><a href="{{ url('/appareils') }}" class="">Appareils</a></li>
  <li><a href="{{ url('/employes') }}">Employés</a></li>
</ul>

@endsection



@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

             <form class="form-inline" method="GET" action="{{ url('/appareils/search') }}">

                {{csrf_field()}}

              <div class="form-group">
                <label for="categorie">Catégories :</label>  
                  <select name="categorie">
                    <option value="ordinateurs">Ordinateurs</option>
                    <option value="smartphones">Smartphones</option>
                    <option value="tablettes">Tablettes</option>
                  </select> 
              </div>

              <div class="form-group">
                <label for="marques">Marques :</label>
                <input type="text" class="form-control" id="marques" name="marques">
              </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-default">Search</button>
              </div>

            </form> 

        </div>
    </div>

      <table class="table">
    <thead>
      <tr>
        <th>Id</th>
        <th>Catégorie</th>
        <th>Marque</th>
        <th>Référence</th>
        <th>Created_at</th>
        <th>Actions</th>
      </tr>
    </thead>

    <tbody>
    @foreach($appareils as $appareil)
      <tr>
        <td>{{$appareil->id}}</td>
        <td>{{$appareil->categorie}}</td>
        <td>{{$appareil->marque}}</td>
        <td><a href="{{url('/appareils/details', [$appareil->id])}}" >{{$appareil->reference}}</a></td>
        <td>{{$appareil->created_at->toFormattedDateString()}}</td>
        <td>

        <a href="{{url('/appareils/download', [$appareil->id])}}" >QR code</a>
        <a href="{{url('/appareils/historique', [$appareil->id])}}" >Historique</a>
        <a href="{{url('/appareils/delete', [$appareil->id])}}" onclick="return confirm('Voulez-vous vraiment supprimez cette appareil ?');">delete</a>

        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>

</div>
@endsection
