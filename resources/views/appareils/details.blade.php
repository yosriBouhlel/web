@extends('layouts.app')

@section('title')

Caractéristiques

@endsection

@section('menus')

<ul class="nav navbar-nav">
  <li class="active" ><a href="{{ url('/appareils') }}" class="">Appareils</a></li>
  <li><a href="{{ url('/employes') }}">Employés</a></li>
</ul>

@endsection

@section('content')
<div class="container">
   <div class="row">
<h1 class="center-block">Caractéristiques :</h1>
</div>
<table class="table">
    <tbody>
        <tr>
          <th>Catégorie</th>
            <td>{{$appareil->categorie}}</td>
        </tr>
        <tr>
          <th>Marque</th>
            <td>{{$appareil->marque}}</td>
        </tr>
        <tr>
          <th>Référence</th>
            <td>{{$appareil->reference}}</td>
        </tr>
      	<tr>
          <th>Microprocesseur</th>
            <td>{{$appareil->microprocesseur}}</td>
        </tr>      	
        <tr>
          <th>Mémoire cache (Mo)</th>
            <td>{{$appareil->cache}}</td>
        </tr>      	
        <tr>
          <th>Ram (Go)</th>
            <td>{{$appareil->ram}}</td>
        </tr>      	
        <tr>
          <th>Disque dur (Go)</th>
            <td>{{$appareil->disque_dur}}</td>
        </tr>      	
        <tr>
          <th>OS</th>
            <td>{{$appareil->os}}</td>
        </tr>      	
        <tr>
          <th>QR code</th>
            <td><a href="{{url('/appareils/download', [$appareil->id])}}" >Télécharger</a></td>
        </tr>              	
        <tr>
          <th>Created_at</th>
            <td>{{$appareil->created_at->toFormattedDateString()}}</td>
        </tr>        
        <tr>
          <th>Disponible</th>
            <td>
            @if($appareil->disponible === 1)
            	OUI
            @else
            	NON
            @endif
            </td>
        </tr>
    </tbody>
</table>


</div>
@endsection