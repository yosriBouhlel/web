@extends('layouts.app')

@section('title')

Liste des employés

@endsection

@section('menus')

<ul class="nav navbar-nav">
  <li  ><a href="{{ url('/appareils') }}" class="">Appareils</a></li>
  <li class="active"><a href="{{ url('/employes') }}">Employés</a></li>
</ul>

@endsection



@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

             <form class="form-inline" method="GET" action="{{ url('/employes/search') }}">

                {{csrf_field()}}

              <div class="form-group">
                <label for="nom">Nom :</label>  
                <input type="text" class="form-control" id="nom" name="nom">
              </div>

              <div class="form-group">
                <label for="prenom">Prénom :</label>
                <input type="text" class="form-control" id="prenom" name="prenom">
              </div>
              <div class="form-group">
                  <button type="submit" class="btn btn-default">Search</button>
              </div>

            </form> 

        </div>
    </div>

    <div class="row">
      <a href="{{ url('/employes/new') }}">Nouveau employé</a>
    </div>

      <table class="table">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Spécialité</th>
        <th>Tél</th>
        <th>Created_at</th>
        <th>Actions</th>
      </tr>
    </thead>

    <tbody>
    @foreach($employes as $employe)
      <tr>
        <td>{{$employe->id}}</td>
        <td>{{$employe->nom}}</td>
        <td>{{$employe->prenom}}</td>
        <td>{{$employe->specialite}}</td>
        <td>{{$employe->tel}}</td>
        <td>{{$employe->created_at->toFormattedDateString()}}</td>
        <td>

        <a href="{{url('/employes/historique', [$employe->id])}}" >Historique</a>
        <a href="{{url('/employes/delete', [$employe->id])}}" onclick="return confirm('Voulez-vous vraiment supprimez ce employé ?');">delete</a>

        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
</div>

</div>
@endsection
