@extends('layouts.app')

@section('title')

Historique de l'employé

@endsection

@section('menus')

<ul class="nav navbar-nav">
  <li  ><a href="{{ url('/appareils') }}" class="">Appareils</a></li>
  <li class="active"><a href="{{ url('/employes') }}">Employés</a></li>
</ul>

@endsection



@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <form class="form-inline" method="GET" action="{{ url('/employes/searchHistorique') }}">

                {{csrf_field()}}

                <div class="form-group">
                <label for="debut">Début :</label>
                    <div class='input-group date'>
                        <input type="text" id="datetimepickerdebut" name="datetimepickerdebut">&nbsp;
                    </div>
                </div>
            
            
                <div class="form-group">
                <label for="fin">Fin :</label>
                    <div class='input-group date'>
                        <input type="text" id="datetimepickerfin" name="datetimepickerfin">&nbsp;
                    </div>
                </div>

                <input type="hidden" name="employe_id" value="<?php if(!$historiques->isEmpty()) echo $historiques{0}->employe_id; ?>" >

              <div class="form-group">
                  <button type="submit" class="btn btn-default">Search</button>
              </div>

            </form>  

        </div>
    </div>

      <table class="table">
    <thead>
      <tr>
        <th>Catégorie</th>
        <th>Marque</th>
        <th>Référence</th>
        <th>Date début</th>
        <th>Date fin</th>
        <th>Actions</th>
      </tr>
    </thead>

    <tbody>

    @if(!$historiques->isEmpty())

    @foreach($historiques as $historique)
      <tr>
      <?php 
        $appareil = App\Appareil::find($historique->appareil_id);
      ?>
        <td>{{$appareil->categorie}}</td>
        <td>{{$appareil->marque}}</td>
        <td><a href="{{url('/appareils/details', [$appareil->id])}}" >{{$appareil->reference}}</a></td>
        <td>{{$historique->debut}}</td>
        <td>{{$historique->fin}}</td>
        <td><a href="{{url('/historiques/delete', [$historique->id])}}" onclick="return confirm('Voulez-vous vraiment supprimez ce historique ?');">delete</a></td>
      </tr>
    @endforeach

    @endif
    </tbody>
  </table>
</div>

</div>
@endsection




@section('javaScript')



  <script>
  $( function() {

    $( "#datetimepickerdebut" ).datepicker({
      altField: "#datetimepickerdebut",
      altFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    }).on("dp.change", function (e) {
            $('#datetimepickerfin').data("DateTimePicker").minDate(e.date);
        });   


    $( "#datetimepickerfin" ).datepicker({
      altField: "#datetimepickerfin",
      altFormat: "yy-mm-dd",
      changeMonth: true,
      changeYear: true
    }).on("dp.change", function (e) {
            $('#datetimepickerdebut').data("DateTimePicker").maxDate(e.date);
        });
  });
  </script>
@endsection

