@extends('layouts.app')

@section('title')

Nouveau employé

@endsection

@section('menus')

<ul class="nav navbar-nav">
  <li  ><a href="{{ url('/appareils') }}" class="">Appareils</a></li>
  <li class="active"><a href="{{ url('/employes') }}">Employés</a></li>
</ul>

@endsection



@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

             <form class="form-horizontal" method="POST" action="{{ url('/employes/store') }}">

                {{csrf_field()}}

                <div class="form-group">
                  <label for="nom">Nom :</label>  
                  <input type="text" class="form-control" id="nom" name="nom">
                </div>

                <div class="form-group">
                  <label for="prenom">Prénom :</label>
                  <input type="text" class="form-control" id="prenom" name="prenom">
                </div> 

                <div class="form-group">
                  <label for="specialite">Spécialité :</label>
                  <input type="text" class="form-control" id="specialite" name="specialite">
                </div>  

                <div class="form-group">
                  <label for="tel">Tél :</label>
                  <input type="text" class="form-control" id="tel" name="tel">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-default">Ajouter</button>
                </div>                

                @if ($errors->any())
                <div class="form-group">
                    <div class="alert alert-danger">
                    <ul>
                    @foreach($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                    </ul>

                    </div>
                </div>
                @endif

            </form> 

        </div>
    </div>
</div>
@endsection
