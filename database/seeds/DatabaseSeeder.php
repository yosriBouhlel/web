<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EmployesTableSeeder::class);
        $this->call(AppareilsTableSeeder::class);
        $this->call(HistoriquesTableSeeder::class);
    }
}
