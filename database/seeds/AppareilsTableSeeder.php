<?php

use Illuminate\Database\Seeder;

use App\Appareil;

class AppareilsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        // Let's truncate our existing records to start from scratch.
        Appareil::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Appareil::create([
                'categorie' => $faker->sentence,
                'marque' => $faker->sentence,
                'reference' => $faker->sentence,
                'microprocesseur' => $faker->sentence,
                'cache' => $faker->numberBetween(1,7),
                'ram' => $faker->numberBetween(1,20),
                'disque_dur' => $faker->numberBetween(10,2000),
                'os' => $faker->sentence,
                'qrcode' => $faker->sentence,
                'disponible' => $faker->boolean,
            ]);
        }

    }
}
